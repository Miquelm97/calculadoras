package com.example.calculadoraanyoshumano;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textEdad; // Text named textAge
    EditText birthYear; // input where we will take age from
    Button calculo;  // Button para hacer el calculo
    String newEdad; // String mostrando la nueva edad
    int anacimiento; // año de nacimiento
    int calculadora; //esto sera para calcular la edad.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Layout de la actividad(el main)
        
        textEdad = findViewById(R.id.tvedad);
        birthYear = findViewById(R.id.etyear);
        calculo = findViewById(R.id.btncalcular);
        /// Cuando clicas al boton ///
        calculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (birthYear.getText().length() == 0 || birthYear.getText().equals(" ")) {
                   //si el campo esta vacio que muestre que introduzcas tu año de nacimiento
                    Toast.makeText(MainActivity.this, "Introduce el año de nacimiento", Toast.LENGTH_LONG).show();
                } else {//Calculate
                    anacimiento = Integer.valueOf(birthYear.getText().toString());
                    calculadora = 2019 - anacimiento;
                    newEdad = "Tienes  " + calculadora + " Años.";
                    textEdad.setText(newEdad);
                    birthYear.clearComposingText();
                }
            }
        });
    }
}
